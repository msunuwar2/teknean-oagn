﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TelerikMvcWebMail.Models;

namespace TelerikMvcWebMail.Controllers
{
    public class CircularController : Controller
    {
        private readonly CircularService circularService;
        public CircularController()
        {
            circularService = new CircularService(new WebApplication.Models.ApplicationDbContext());
         
        }

        public ActionResult Index()
        {
            return View(circularService.GetAll());
        }
        

    }
}
