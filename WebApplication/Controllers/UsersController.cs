﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using WebApplication.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Data.Entity;

namespace WebApplication.Controllers
{
	//[Authorize]
	public class UsersController : Controller
    {

       

        // GET: Users
        public Boolean isAdminUser()
		{
			if (User.Identity.IsAuthenticated)
			{
				var user = User.Identity;
				ApplicationDbContext context = new ApplicationDbContext();
				var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
				var s = UserManager.GetRoles(user.GetUserId());
				if (s[0].ToString() == "Admin")
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			return false;
		}
		public ActionResult Index1()
		{
			if (User.Identity.IsAuthenticated)
			{
				var user = User.Identity;
				ViewBag.Name = user.Name;
				//	ApplicationDbContext context = new ApplicationDbContext();
				//	var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

				//var s=	UserManager.GetRoles(user.GetUserId());
				ViewBag.displayMenu = "No";

				if (isAdminUser())
				{
					ViewBag.displayMenu = "Yes";
				}
				return View();
			}
			else
			{
				ViewBag.Name = "Not Logged IN";
			}


			return View();
              }

        public ActionResult Index()
        {
            return View("Users");
        }

        public ActionResult Users_Read([DataSourceRequest] DataSourceRequest request)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            
            return Json(UserManager.Users.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        public ActionResult Picture(string id)
        {
            if (id != null)
            {
                return base.File("../../Content/contacts/" + id + ".jpg", "image/jpeg");
            }
            else
            {
                return base.File("../../Content/contacts/" + "default-contact.png", "image/jpeg");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> EditingPopup_Create([DataSourceRequest] DataSourceRequest request, RegisterViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {

                var user = new ApplicationUser { UserName = userViewModel.Email, Email = userViewModel.Email };

                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));


                var adminresult =await  UserManager.CreateAsync(user, userViewModel.Password = "Admin123!@#");
                //Add User to the selected Roles 
                if (adminresult.Succeeded)
                {
                    if (selectedRoles != null)
                    {
                        var result = await UserManager.AddToRolesAsync(user.Id, selectedRoles);
                        if (!result.Succeeded)
                        {
                            ModelState.AddModelError("", result.Errors.First());
                            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
                            return View();
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", adminresult.Errors.First());
                    ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
                    return View();

                }
            }
          
            return Json(new[] { userViewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> EditingPopup_Update([DataSourceRequest] DataSourceRequest request,  ApplicationUser userViewModel, params string[] selectedRole)
        {
            if (ModelState.IsValid)
            {
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));


                var user = await UserManager.FindByIdAsync(userViewModel.Id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                user.UserName = userViewModel.Email;
                user.Email = userViewModel.Email;
                user.PhoneNumber = userViewModel.PhoneNumber;
                user.EmailConfirmed = userViewModel.EmailConfirmed;

                var userRoles = await UserManager.GetRolesAsync(user.Id);

                selectedRole = selectedRole ?? new string[] { };

                var result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray<string>());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray<string>());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", "Something failed.");

          
            return Json(new[] { userViewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> EditingPopup_Destroy([DataSourceRequest] DataSourceRequest request, ApplicationUser userViewModel)
        {
            if (ModelState.IsValid)
            {
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));


                var user = await UserManager.FindByIdAsync(userViewModel.Id);

                if (user == null)
                {
                    return HttpNotFound();
                }

                var result = await UserManager.DeleteAsync(user);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }

            }
            ////===================================================================================================================
            //string sX = userViewModel.GetStringWith_RecordProperties();
            ////===================================================================================================================
            //var logRecord = new WriteUsageLog();
            //logRecord.Note = "New WorkFlow Record Added - " + sX;

            //logRecord.UserLogIn = General_ActiveDirectory_Extensions.fn_sUser();
            //string IP = Request.UserHostName;
            //logRecord.ComputerName = General_functions.fn_ComputerName(IP);

            //_writeUsageLogService.Create(logRecord);
            ////===================================================================================================================

            return Json(new[] { userViewModel }.ToDataSourceResult(request, ModelState));
        }

    }
}