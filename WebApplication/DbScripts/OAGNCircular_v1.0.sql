USE [OAGNCircular]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tasks_Tasks]') AND parent_object_id = OBJECT_ID(N'[dbo].[Events]'))
ALTER TABLE [dbo].[Events] DROP CONSTRAINT [FK_Tasks_Tasks]
GO
/****** Object:  Index [IX_FK_Tasks_Tasks]    Script Date: 5/8/2017 8:58:23 AM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Events]') AND name = N'IX_FK_Tasks_Tasks')
DROP INDEX [IX_FK_Tasks_Tasks] ON [dbo].[Events]
GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 5/8/2017 8:58:23 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tasks]') AND type in (N'U'))
DROP TABLE [dbo].[Tasks]
GO
/****** Object:  Table [dbo].[People]    Script Date: 5/8/2017 8:58:23 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[People]') AND type in (N'U'))
DROP TABLE [dbo].[People]
GO
/****** Object:  Table [dbo].[Mails]    Script Date: 5/8/2017 8:58:23 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Mails]') AND type in (N'U'))
DROP TABLE [dbo].[Mails]
GO
/****** Object:  Table [dbo].[Events]    Script Date: 5/8/2017 8:58:23 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Events]') AND type in (N'U'))
DROP TABLE [dbo].[Events]
GO
USE [master]
GO
/****** Object:  Database [OAGNCircular]    Script Date: 5/8/2017 8:58:23 AM ******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'OAGNCircular')
DROP DATABASE [OAGNCircular]
GO
/****** Object:  Database [OAGNCircular]    Script Date: 5/8/2017 8:58:23 AM ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'OAGNCircular')
BEGIN
	CREATE DATABASE [OAGNCircular]
END
GO
ALTER DATABASE [OAGNCircular] SET COMPATIBILITY_LEVEL = 100
GO

USE [OAGNCircular]
GO
/****** Object:  Table [dbo].[Events]    Script Date: 5/8/2017 8:58:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Events]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Events](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[Start] [datetime] NOT NULL,
	[End] [datetime] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Category] [int] NULL,
	[IsAllDay] [bit] NOT NULL,
	[RecurrenceRule] [nvarchar](max) NULL,
	[RecurrenceID] [int] NULL,
	[RecurrenceException] [nvarchar](max) NULL,
	[StartTimezone] [nvarchar](max) NULL,
	[EndTimezone] [nvarchar](max) NULL,
 CONSTRAINT [PK_Events] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Mails]    Script Date: 5/8/2017 8:58:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Mails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Mails](
	[MessageID] [int] IDENTITY(1,1) NOT NULL,
	[Body] [nvarchar](max) NULL,
	[From] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[Subject] [nvarchar](256) NULL,
	[Received] [datetime] NULL,
	[IsRead] [bit] NULL,
	[To] [nvarchar](256) NULL,
	[Category] [nvarchar](50) NULL,
 CONSTRAINT [PK_Mails] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[People]    Script Date: 5/8/2017 8:58:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[People]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[People](
	[Id] [nchar](5) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[Country] [nvarchar](25) NULL,
	[City] [nvarchar](30) NULL,
	[Company] [nvarchar](40) NULL,
	[Phone] [nvarchar](30) NULL,
	[Email] [nvarchar](50) NULL,
	[Category] [nvarchar](50) NULL,
 CONSTRAINT [PK_People] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 5/8/2017 8:58:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tasks]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Tasks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](256) NULL,
	[DateCreated] [datetime] NULL,
	[NoteContent] [nvarchar](500) NULL,
	[Category] [nvarchar](50) NULL,
 CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Events] ON 

GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (4, CAST(N'2013-06-09T21:00:00.000' AS DateTime), CAST(N'2013-06-10T00:00:00.000' AS DateTime), N'Bowling tournament', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (5, CAST(N'2013-06-10T07:00:00.000' AS DateTime), CAST(N'2013-06-10T08:00:00.000' AS DateTime), N'Take the dog to the vet', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (6, CAST(N'2013-06-11T11:30:00.000' AS DateTime), CAST(N'2013-06-11T13:00:00.000' AS DateTime), N'Call Charlie about the project', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (7, CAST(N'2013-06-12T08:00:00.000' AS DateTime), CAST(N'2013-06-12T09:00:00.000' AS DateTime), N'Meeting with Alex', NULL, 4, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (9, CAST(N'2013-06-14T02:00:00.000' AS DateTime), CAST(N'2013-06-14T02:00:00.000' AS DateTime), N'Alex''s Birthday', N'', 2, 1, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (12, CAST(N'2013-06-24T08:30:00.000' AS DateTime), CAST(N'2013-06-24T09:30:00.000' AS DateTime), N'Car Service', N'Might come to work later!', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (14, CAST(N'2013-06-24T10:00:00.000' AS DateTime), CAST(N'2013-06-24T11:00:00.000' AS DateTime), N'Replace the printer on the 1st floor', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (15, CAST(N'2013-06-25T00:00:00.000' AS DateTime), CAST(N'2013-06-26T00:00:00.000' AS DateTime), N'Attending HR Conference', N'', 1, 1, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (16, CAST(N'2013-06-25T12:00:00.000' AS DateTime), CAST(N'2013-06-25T13:00:00.000' AS DateTime), N'Business Lunch with Gregory Watkins', N'', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (17, CAST(N'2013-06-27T08:30:00.000' AS DateTime), CAST(N'2013-06-27T09:30:00.000' AS DateTime), N'Breakfast with CFO and COO', N'', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (18, CAST(N'2013-06-27T10:00:00.000' AS DateTime), CAST(N'2013-06-27T11:00:00.000' AS DateTime), N'Job Interview - Mathew Stevens', N'Junior Researcher', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (19, CAST(N'2013-06-27T11:00:00.000' AS DateTime), CAST(N'2013-06-27T11:30:00.000' AS DateTime), N'Review CVs with Tim', N'', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (20, CAST(N'2013-06-27T12:00:00.000' AS DateTime), CAST(N'2013-06-27T13:30:00.000' AS DateTime), N'Lunch with Monica', N'Discuss the Employee handbook', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (21, CAST(N'2013-06-27T14:00:00.000' AS DateTime), CAST(N'2013-06-27T15:00:00.000' AS DateTime), N'Job Interview - John Stewart', N'Accountant', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (22, CAST(N'2013-06-27T15:30:00.000' AS DateTime), CAST(N'2013-06-27T16:30:00.000' AS DateTime), N'Job Interview - Mary Smith', N'Accountant', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (24, CAST(N'2013-06-24T12:00:00.000' AS DateTime), CAST(N'2013-06-24T12:30:00.000' AS DateTime), N'Register new Access Cards', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (25, CAST(N'2013-06-04T16:00:00.000' AS DateTime), CAST(N'2013-06-04T18:00:00.000' AS DateTime), N'HR Lecture', NULL, 5, 0, N'FREQ=WEEKLY;BYDAY=TU,TH', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (26, CAST(N'2013-06-28T08:00:00.000' AS DateTime), CAST(N'2013-06-28T09:00:00.000' AS DateTime), N'Dentist', N'', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (27, CAST(N'2013-06-28T09:30:00.000' AS DateTime), CAST(N'2013-06-28T10:30:00.000' AS DateTime), N'Job Interview - Laura Bailey', N'Helpdesk', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (28, CAST(N'2013-06-28T11:00:00.000' AS DateTime), CAST(N'2013-06-28T12:00:00.000' AS DateTime), N'Job Interview - Jenny Baxter', N'Helpdesk', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (31, CAST(N'2013-06-28T14:00:00.000' AS DateTime), CAST(N'2013-06-28T17:00:00.000' AS DateTime), N'Team building prep tasks', N'', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (32, CAST(N'2013-06-24T13:30:00.000' AS DateTime), CAST(N'2013-06-24T14:30:00.000' AS DateTime), N'Job Interview - Bernard Atkins', N'Helpdesk', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (34, CAST(N'2013-06-24T15:00:00.000' AS DateTime), CAST(N'2013-06-24T17:30:00.000' AS DateTime), N'Review Job Applications', N'', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (35, CAST(N'2013-06-23T00:00:00.000' AS DateTime), CAST(N'2013-06-23T00:00:00.000' AS DateTime), N'Grand Canyon tour', N'', 1, 1, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (40, CAST(N'2013-06-24T13:30:00.000' AS DateTime), CAST(N'2013-06-24T18:00:00.000' AS DateTime), N'Install new laptops in conference rooms', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (66, CAST(N'2013-06-29T08:00:00.000' AS DateTime), CAST(N'2013-06-29T06:00:00.000' AS DateTime), N'Bob''s Birthday', N'', 3, 1, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (68, CAST(N'2013-06-24T08:30:00.000' AS DateTime), CAST(N'2013-06-24T09:00:00.000' AS DateTime), N'Breakfast with Tom', N'', 1, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (69, CAST(N'2013-06-24T10:00:00.000' AS DateTime), CAST(N'2013-06-24T12:00:00.000' AS DateTime), N'Team planning meeting', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (70, CAST(N'2013-06-24T16:00:00.000' AS DateTime), CAST(N'2013-06-24T16:30:00.000' AS DateTime), N'Support Phone Call', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (71, CAST(N'2013-06-25T09:00:00.000' AS DateTime), CAST(N'2013-06-25T10:00:00.000' AS DateTime), N'Business breakfast with Caroline', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (72, CAST(N'2013-06-25T11:00:00.000' AS DateTime), CAST(N'2013-06-25T11:30:00.000' AS DateTime), N'Discuss preojects'' deadlines', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (73, CAST(N'2013-06-25T15:00:00.000' AS DateTime), CAST(N'2013-06-25T16:00:00.000' AS DateTime), N'Support Meeting', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (74, CAST(N'2013-06-25T18:30:00.000' AS DateTime), CAST(N'2013-06-25T20:00:00.000' AS DateTime), N'Dine with Mathew', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (79, CAST(N'2013-06-26T09:00:00.000' AS DateTime), CAST(N'2013-06-26T10:00:00.000' AS DateTime), N'Banking', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (80, CAST(N'2013-06-25T10:00:00.000' AS DateTime), CAST(N'2013-06-25T12:00:00.000' AS DateTime), N'Software updates', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (81, CAST(N'2013-06-25T16:30:00.000' AS DateTime), CAST(N'2013-06-25T18:00:00.000' AS DateTime), N'UPS maintenance', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (82, CAST(N'2013-06-26T11:30:00.000' AS DateTime), CAST(N'2013-06-26T12:00:00.000' AS DateTime), N'Support Call', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (83, CAST(N'2013-06-26T13:30:00.000' AS DateTime), CAST(N'2013-06-26T14:30:00.000' AS DateTime), N'Phone Sync with NY office ', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (84, CAST(N'2013-06-26T15:00:00.000' AS DateTime), CAST(N'2013-06-26T16:00:00.000' AS DateTime), N'Phone Sync with Boston Office', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (85, CAST(N'2013-06-26T16:30:00.000' AS DateTime), CAST(N'2013-06-26T18:30:00.000' AS DateTime), N'Server maintenance', NULL, 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (86, CAST(N'2013-06-28T13:30:00.000' AS DateTime), CAST(N'2013-06-28T15:30:00.000' AS DateTime), N'Status meeting', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (87, CAST(N'2013-06-27T10:30:00.000' AS DateTime), CAST(N'2013-06-27T11:30:00.000' AS DateTime), N'Helpdesk status meeting', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (88, CAST(N'2013-06-27T12:00:00.000' AS DateTime), CAST(N'2013-06-27T13:00:00.000' AS DateTime), N'Business Lunch', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (89, CAST(N'2013-06-27T14:00:00.000' AS DateTime), CAST(N'2013-06-27T15:30:00.000' AS DateTime), N'Employee database update', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (90, CAST(N'2013-06-27T07:30:00.000' AS DateTime), CAST(N'2013-06-27T08:30:00.000' AS DateTime), N'Website upload', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (91, CAST(N'2013-06-27T17:00:00.000' AS DateTime), CAST(N'2013-06-27T18:30:00.000' AS DateTime), N'Meeting with marketing guys', N'', 2, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (92, CAST(N'2013-06-28T10:30:00.000' AS DateTime), CAST(N'2013-06-28T11:30:00.000' AS DateTime), N'Meeting with Internet provider', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (93, CAST(N'2013-06-29T20:00:00.000' AS DateTime), CAST(N'2013-06-29T23:30:00.000' AS DateTime), N'Bob''s Birthday Party', N'', 3, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (95, CAST(N'2013-06-03T15:30:00.000' AS DateTime), CAST(N'2013-06-03T17:00:00.000' AS DateTime), N'Dance Practice', NULL, 4, 0, N'FREQ=WEEKLY;BYDAY=MO,WE', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (114, CAST(N'2013-06-04T06:00:00.000' AS DateTime), CAST(N'2013-06-04T09:00:00.000' AS DateTime), N'Software updates', NULL, 5, 0, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (115, CAST(N'2013-06-04T08:00:00.000' AS DateTime), CAST(N'2013-06-04T09:30:00.000' AS DateTime), N'Breakfast at Starbucks', N'', 1, 0, N'', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (116, CAST(N'2013-06-04T14:00:00.000' AS DateTime), CAST(N'2013-06-04T17:00:00.000' AS DateTime), N'Performance review', N'', 2, 0, N'', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (118, CAST(N'2013-06-05T10:00:00.000' AS DateTime), CAST(N'2013-06-05T12:00:00.000' AS DateTime), N'HR seminar preparation', N'', 1, 0, N'', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (119, CAST(N'2013-06-05T15:00:00.000' AS DateTime), CAST(N'2013-06-05T16:00:00.000' AS DateTime), N'Helpdesk weekly meeting', N'', 3, 0, N'FREQ=WEEKLY;BYDAY=WE', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Events] ([EventID], [Start], [End], [Title], [Description], [Category], [IsAllDay], [RecurrenceRule], [RecurrenceID], [RecurrenceException], [StartTimezone], [EndTimezone]) VALUES (120, CAST(N'2013-06-07T07:00:00.000' AS DateTime), CAST(N'2013-06-07T08:30:00.000' AS DateTime), N'Website upload', N'', 3, 0, N'', NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Events] OFF
GO
SET IDENTITY_INSERT [dbo].[Mails] ON 

GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (1, N'I am using the RadCloud upload with Azure blob storage.&nbsp;<br />
<br />
When I upload a file it gets copied into Azure Blob storage as expected. The user then hits a ''START'' button and the file gets used as part of my workflow, (i.e. the radcloud list gets emptied and i remove all files from azure). This is all fine.<br />
<br />
My problem is if the user decides to refresh the page before hitting the ''START'' button. When the page refreshes, the&nbsp;cloud upload list will get refreshed (i.e. emptied) but the file remains in blob storage. So the radcloud upload list is nolonger in sync with what is on azure.<br />
<br />
<br />
My question is<br />
<br />
1) how do i keep the radcloud file list in sync with the files that are in azure.<br />
<br />
2) if i added code to pull the list from azure, how do i add this list to the radcloudupload list<br />
<br />
<br />
thanks,<br />
michael.', N'Maria Anders', N'Maria.Anders@telerik.com', N'Keep RadcloudUpload list  synced with Azure backend', CAST(N'2015-08-08T19:39:00.000' AS DateTime), 1, N'Antonio Morandi', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (2, N'Hello,<br/>1) Is there a way to do an automatic post-back when files finish uploading without the user having to click on a button? I am trying to save uploaded file information to database for further processing. The event doe not seem to be firing.<br/>Thanks.', N'Christina Berglund', N'Christina.Berglund@telerik.com', N'RadCloudUpload1.FileUploaded Not firing', CAST(N'2015-08-07T20:41:00.000' AS DateTime), 1, N'Antonio Morandi; Viktor Tachev', N'Deleted')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (4, N'Hi,<br />
we''ve found an issue in IE 11 using the RacCloudUpload and an Azure custom upload handler.<br />
<br />
With Chrome is working properly but we''ve detected this behaviour in IE.<br />
<br />
Steps to reproduce the issue:<br />
<br />
* Visit a page with the control loaded.<br />
* Upload a file<br />
* Do an iisreset<br />
* Without refresh the page, try to upload another file.<br />
* In Chrome it works properly but in IE the process hangs and begins to consume more CPU.&nbsp;<br />
<br />
Finally we have to kill the IE process from the task manager.<br />
<br />
The issue doesn''t happen if after the iisreset we refresh the web page.<br />
<br />
Obviously, the iisreset is not going to happen often, but I don''t understand why in Chrome is working but not in IE.<br />
Do you have any suggestion?<br />
<br />
King regards', N'Pedro Afonso', N'Pedro.Afonso@telerik.com', N'IE Freeze after iisreset', CAST(N'2015-07-03T03:21:00.000' AS DateTime), 1, N'Antonio Morandi; Vasil Reav', N'Deleted')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (5, N'Hello,<br />
<br />
Thank you for contacting Telerik Support.<br />
<br />
There is an issue with the progress indicator which shows 100%, but the actual uploading is working. Can you try to review all requests that the CloudUpload makes using Fiddler or IE Network tools? Is the file uploaded if you wait longer?&nbsp;<br />
<br />
When file is selected control automatically uploads it into azure, but it also creates a callback into the application cache. If you cause a postback the file will be submitted to the Azure servers, however if you don''t submit it the callback will be executed and the file will be removed from Azure servers. When you reset or recycle the IIS, the application cache will be lost. Is there any reason to reset the IIS?&nbsp;<br />
<br />
I''m looking forward to hearing from you.<br />
<br />
Regards,<br />
Hristo Valyavicharski <br />
Telerik <br/>Do you want to have your say when we set our development plans? Do you want to know when a feature you care about is added or when a bug fixed? Explore the <a href="http://www.telerik.com/support/feedback" target="_blank">Telerik Feedback Portal</a> and vote to affect the priority of the items&nbsp;', N'Hanna Moos', N'Hanna.Moos@telerik.com', N'RE: IE Freeze after iisreset', CAST(N'2015-07-06T03:35:00.000' AS DateTime), 1, N'Antonio Morandi; Viktor Tachev; Ricardo Chirra; Mahesh Machineni', N'Deleted')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (6, N'Hi, thank you for your answer.<br />
<br />
In a normal situation, we are uploading files and after that we are doing a postback to force the callback event in the backend. When the file size is over 1MB the progress bar is near the 100%, but that is not the issue.<br />
<br />
When the IE process hangs, the file is not uploaded. Simply the process hangs and the unique way to recover is to kill the process in the Task Manager.<br />
<br />
The IIS reset shouldn''t occur in a normal situation, but we are using the Telerik control with Sharepoint, and for example if we are deploying a new version of some sharepoint package, an iisreset will be done.&nbsp;<br />
Actually, we have found the bug that way.<br />
<br />
An hypothetical case:<br />
<br />
* A client is visiting the site and he has the browser opened in the page to use the control, but he doesn''t close the browser.<br />
<br />
* We schedule a deploy to the production environment and after do it, we do an iisreset.<br />
<br />
* A time after, the client try to upload a file in the same browser instance, in this case, the IE process will hang and the user will have to killl the IE process.<br />
<br />
King regards', N'Pedro Afonso', N'Pedro.Afonso@telerik.com', N'RE: RE: IE Freeze after iisreset', CAST(N'2015-07-06T04:18:00.000' AS DateTime), 1, N'Antonio Morandi; Ricardo Chirra; Vasil Reav', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (7, N'Thank you for getting back to us. 

The RadCloudUpload cannot handle this case. How is configured your session? Doesn''t it expire after resetting the IIS? Doesn''t it propmpt you to enter your credentilals again? What you if have other control that makes ajax request? Does it work?

Regards,
Hristo Valyavicharski 
Telerik 
Do you want to have your say when we set our development plans? Do you want to know when a feature you care about is added or when a bug fixed? Explore the Telerik Feedback Portal and vote to affect the priority of the items 
', N'Hanna Moos', N'Hanna.Moos@telerik.com', N'RE: RE: RE: IE Freeze after iisreset', CAST(N'2015-07-06T06:41:00.000' AS DateTime), 1, N'Antonio Morandi; Vasil Reav', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (8, N'How can the last node in a RadNavigation control be right-aligned? <br />
<br />
Referring to the attached picture and the HTML below I''d like to right-align the "navNodeSearchBox" node within the RadNavigation control while all other items remain left-aligned.<br />
<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div id="divNavigation" class="pageWidthElementPlus5px" style="vertical-align:top; display:block; margin-left:-1px; text-align:left " &gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;telerik:RadNavigation ID="radNavigation1" runat="server" MenuButtonPosition="Right" BorderStyle="None" BorderColor="Transparent" BorderWidth="0px" &gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;Nodes&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;telerik:NavigationNode ID="navNodeHome" Text="Home" runat="server"&gt;&lt;/telerik:NavigationNode&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;telerik:NavigationNode ID="navNodeCategories" Text="Categories" runat="server"&gt;&lt;/telerik:NavigationNode&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;telerik:NavigationNode ID="navNodeSearch" Text="Search" runat="server"&gt;&lt;/telerik:NavigationNode&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;telerik:NavigationNode ID="navNodeView" Text="View" runat="server"&gt;&lt;/telerik:NavigationNode&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;telerik:NavigationNode ID="navNodeAbout" Text="About" runat="server"&gt;&lt;/telerik:NavigationNode&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;telerik:NavigationNode ID="navNodeMyAccount" Text="My Account" runat="server"&gt;&lt;/telerik:NavigationNode&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;telerik:NavigationNode ID="navNodeSearchBox" Text="search videos" runat="server" CssClass="searchBoxWrapper" Width="400"&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;NodeTemplate&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;telerik:RadSearchBox ID="RadSearchBox1" runat="server" Width="370"&gt;&lt;/telerik:RadSearchBox&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/NodeTemplate&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/telerik:NavigationNode&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/Nodes&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/telerik:RadNavigation&gt;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;', N'Martin Sommer', N'Martín.Sommer@telerik.com', N'Right-Align Last Navigation Node', CAST(N'2015-03-08T18:10:00.000' AS DateTime), 1, N'Antonio Morandi; Mahesh Machineni', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (9, N'If radcombobox contains items with special characters means non English characters like &ccedil;&atilde;<br />
<br />
Radcombobox is not closing after filtering, it &nbsp;remains opened always and focus is not going out of the control, if focus is moving out i.e. if i press F12 for developer tools it is working<br />
<br />
Please suggest a fix for this.', N'Laurence Lebihan', N'Laurence.Lebihan@telerik.com', N'RadCombobox is not closing in IE11 after filtering,it stays opened always', CAST(N'2015-03-02T04:08:00.000' AS DateTime), 1, N'Antonio Morandi', N'Junk')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (11, N'Hi <span class="text">Eric</span>,<br />
<br />
Thank you for contacting us.<br />
<br />
I just tested the rebind method using batch editing with client data source. You can check it online in our demo:<br />
<a href="http://demos.telerik.com/aspnet-ajax/grid/examples/data-editing/batch-editing/defaultcs.aspx" target="_blank">http://demos.telerik.com/aspnet-ajax/grid/examples/data-editing/batch-editing/defaultcs.aspx</a><br />
Open the browser console and write:</span></p>
<p style="background: white;"><code><span style="font-size: 10pt;">$find(</span></code><code><span style="color: blue; font-size: 10pt;">"ctl00_ContentPlaceholder1_RadGrid1"</span></code><code><span style="font-size: 10pt;">).get_masterTableView().rebind()</span></code></p>
<p><span style="font-size: 9pt; font-family: Arial, sans-serif;">It seems to work correct, and if I am not wrong it is in the same scenario that you are using it.<br />
<br />
If you continue to have problems, please try to insulate the problem in sample page and send it for further debugging.<br />
<br />
Regards,<br />
Vasil <br />
Telerik', N'Elizabeth Lincoln', N'Elizabeth.Lincoln@telerik.com', N'RE: ClientDataSource / Grid : javascript rebind error with Version 2015.1.225', CAST(N'2015-03-09T01:59:00.000' AS DateTime), 1, N'Antonio Morandi; Vasil Reav', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (12, N'Hi, I have a grid connected to a clientdatasource and I need to refresh the grid from the web service, how can I do that using client side methods?
I tried the fetch() method of the datasource, or rebind() from the master table view but nothing works.
Thanks.
', N'Patricio Simpson', N'Patricio.Simpson@telerik.com', N' Client side refresh', CAST(N'2015-01-15T12:16:00.000' AS DateTime), 1, N'Antonio Morandi', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (13, N'<ul style="list-style-type: disc;">
    <li>RadGridClientDataSourceSample.zip</li>
</ul>
<p>Hello <span class="messagemaininfo">Ricardo,<br />
<br />
For your convenience I have prepared a sample WebSite Project that illustrates how you can rebind RadGrid when client-side binding is used. In the sample the data is different based on the selected item in the RadTreeView control. <br />
<br />
Try using similar approach and you should be able to implement the functionality you are looking for.<br />
<br />
Regards,<br />
Viktor Tachev <br />
Telerik', N'Francisco Chang', N'Francisco.Chang@telerik.com', N'RE: Client side refresh', CAST(N'2015-01-20T08:17:00.000' AS DateTime), 1, N'Antonio Morandi; Vasil Reav; Mahesh Machineni', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (14, N'Is it possible to combine filtering and web service binding within the same Rad Drop Down Tree control?<br />
<br />
Thanks in advance for your answer,<br />
<br />
Giorgos', N'Yang Wang', N'Yang.Wang@telerik.com', N'Filtering together with web service binding', CAST(N'2013-11-27T13:13:00.000' AS DateTime), 1, N'Antonio Morandi; Vasil Reav', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (15, N'I have just tried it out. <br />
<br />
I got the message "It is not possible to combine Web Service Binding and Filtering".<br />
<br />
Hope this will save some time from anybody that might have a similar question.<br />
<br />
Giorgos</span><span style="font-size: 9pt; font-family: Arial, sans-serif;">Is it possible to combine filtering and web service binding within the same Rad Drop Down Tree control?<br />
<br />
Thanks in advance for your answer,<br />
<br />
Giorgos', N'Yang Wang', N'Yang.Wang@telerik.com', N'RE: Filtering together with web service binding', CAST(N'2013-11-29T05:59:00.000' AS DateTime), 1, N'Antonio Morandi; Ricardo Chirra', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (16, N'Hi Giorgos,<br />
<br />
To work the filtering and the web service binding the response of the web service should be hierarchical data and now it is not possible to be integrated with the current implementation(flat structure is returned). This is part of our future plans for the control.<br />
<br />
Regards,<br />
Peter Filipov <br />
Telerik </span></p>
<p><span style="font-size: 9pt; font-family: Arial, sans-serif;">If you want to get updates on new releases, tips and tricks and sneak peeks at our product labs directly from the developers working on the RadControls for ASP.NET AJAX, subscribe to the <a href="http://feeds.feedburner.com/AspnetAjaxTeamsFeed?utm_source=SupportFooters&amp;utm_medium=banner&amp;utm_campaign=AJAX_blogs_supportfooters">blog feed</a> now.&nbsp;</span></p>
<p><span style="font-size: 9pt; font-family: Arial, sans-serif;">I have just tried it out. <br />
<br />
I got the message "It is not possible to combine Web Service Binding and Filtering".<br />
<br />
Hope this will save some time from anybody that might have a similar question.<br />
<br />
Giorgos</span><span style="font-size: 9pt; font-family: Arial, sans-serif;">Is it possible to combine filtering and web service binding within the same Rad Drop Down Tree control?<br />
<br />
Thanks in advance for your answer,<br />
<br />
Giorgos', N'Sven Ottlieb', N'Sven.Ottlieb@telerik.com', N'RE: RE: Filtering together with web service binding', CAST(N'2013-12-02T03:50:00.000' AS DateTime), 1, N'Antonio Morandi; Vasil Reav; Mahesh Machineni', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (17, N'So I have a RadGrid with an ImageButton in a GridTemplateColumn.&nbsp; Also on this page is a RadAjaxManager, a RadWindowManager&nbsp;and a RadScriptManager.&nbsp; There is no MasterPage.<br />

<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">RadGrid</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> ID</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="RadGrid1"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> runat</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="server"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> AllowPaging</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="True"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> AllowSorting</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="True"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> AutoGenerateColumns</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="False" </span><span style="font-size: 10pt; font-family: Consolas; color: red;">GroupPanelPosition</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Top" </span><span style="font-size: 10pt; font-family: Consolas; color: red;">ShowStatusBar</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="True"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> Skin</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Office2010Blue"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> DataKeyNames</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="OID"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> OnItemCommand</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="RadGrid1_ItemCommand"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> OnNeedDataSource</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="RadGrid1_NeedDataSource"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> CellSpacing</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="-1"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">GridLines</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Both"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> ViewStateMode</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Enabled"&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">MasterTableView</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">Columns</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: red;">DataField</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Purchased" </span><span style="font-size: 10pt; font-family: Consolas; color: red;">FilterControlAltText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Filter column1 column"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">HeaderText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Purchased" </span><span style="font-size: 10pt; font-family: Consolas; color: red;">UniqueName</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="column1"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> DataFormatString</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="{0:d/M/yyyy}"&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> DataField</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Customer"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> FilterControlAltText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Filter column2 column"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> HeaderText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Customer"</span><span style="font-size: 10pt; font-family: Consolas; color: red;"> UniqueName</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="column2"&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: red;">DataField</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Product"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">FilterControlAltText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Filter column3 column"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">HeaderText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Product"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">UniqueName</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="column3"&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: red;">DataField</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Check"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">FilterControlAltText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Filter column4 column"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">HeaderText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Check"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">UniqueName</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="column4"&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: red;">DataField</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Disbursed"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">FilterControlAltText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Filter column5 column"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">HeaderText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Dispersed"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">UniqueName</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="column5"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">DataFormatString</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="{0:d/M/yyyy}"&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: red;">DataField</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Salesperson"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">FilterControlAltText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Filter column6 column"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">HeaderText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Salesperson"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">UniqueName</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="column6"&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: red;">DataField</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Status"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">FilterControlAltText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Filter column10 column"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">HeaderText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Status"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">UniqueName</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="column10"&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridBoundColumn</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridTemplateColumn</span><span style="font-size: 10pt; font-family: Consolas; color: red;">FilterControlAltText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Filter AgreementCol column"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">UniqueName</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="AgreementCol"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">HeaderText</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="Agr"&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">ItemTemplate</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">asp</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">ImageButton</span><span style="font-size: 10pt; font-family: Consolas; color: red;">ID</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="AgreementPDFIB"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">runat</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="server"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">ImageUrl</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="~/images/Icons/pdf.gif"</span><span style="font-size: 10pt; font-family: Consolas; color: red;">style</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="</span><span style="font-size: 10pt; font-family: Consolas; color: red;">display</span><span style="font-size: 10pt; font-family: Consolas;">: <span style="color: blue;">inline-block"</span><span style="color: red;">ToolTip</span><span style="color: blue;">="Open PPS Agreement"</span><span style="color: red;">CommandName</span><span style="color: blue;">="OpenAgreePDF"</span><span style="color: red;">CommandArgument</span><span style="color: blue;">=''</span>&lt;%<span style="color: blue;"># Eval("OID") </span>%&gt;<span style="color: blue;">''/&gt;</span></span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">ItemTemplate</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">HeaderStyle</span><span style="font-size: 10pt; font-family: Consolas; color: red;">Width</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">="15px"/&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">GridTemplateColumn</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">Columns</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">MasterTableView</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: blue;">&lt;/</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">telerik</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">:</span><span style="font-size: 10pt; font-family: Consolas; color: maroon;">RadGrid</span><span style="font-size: 10pt; font-family: Consolas; color: blue;">&gt;</span></p>
<p><span style="font-size: 9pt; font-family: Arial, sans-serif;">The user clicks on the PDF image and this triggers the CommandName argument which calls the following code to open the PDF:<br />
</span></p>
<p><span style="font-size: 10pt; font-family: Consolas;">RadAjaxManager1.Redirect(<span style="color: #a31515;">"PrintPDF.aspx?roid="</span> + ReportTypeOID);</span></p>
<p><span style="font-size: 9pt; font-family: Arial, sans-serif;">The issue happens only on an iPad in Safari.&nbsp; The problem is that the PDF opens in the same window and once it opens, the user is stuck on the PDF unable to escape.&nbsp; In all other browsers on the desktop, the PDF will open in a separate window or tab.<br />
<br />
Early on we had issues with this and discovered that the headers were instrumental in opening the PDF properly.&nbsp; Only the following combination would open a PDF in a separate window on an iPad:<br />
</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: #2b91af;">HttpContext</span><span style="font-size: 10pt; font-family: Consolas;">.Current.Response.AddHeader(<span style="color: #a31515;">"Content-Disposition"</span>, <span style="color: #a31515;">"attachment;filename=PPSAgreement.pdf"</span>);</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: #2b91af;">HttpContext</span><span style="font-size: 10pt; font-family: Consolas;">.Current.Response.AddHeader(<span style="color: #a31515;">"Content-Length"</span>, bytes.Length.ToString());</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: #2b91af;">HttpContext</span><span style="font-size: 10pt; font-family: Consolas;">.Current.Response.ContentType = <span style="color: #a31515;">"application/octet-stream"</span>;</span></p>
<p><span style="font-size: 10pt; font-family: Consolas; color: #2b91af;">HttpContext</span><span style="font-size: 10pt; font-family: Consolas;">.Current.Response.BinaryWrite(bytes);</span></p>
Specifying a ContentType = ''application/pdf'' would cause the pdf to open in the same window for hyperlinks.&nbsp; ''octet-stream'' would allow the pdf to open in a separate window.&nbsp; <br />
<br />
All pages have the same design.&nbsp; They all use redirects and all point to the same file:&nbsp; PrintPDF.aspx.cs.&nbsp; All pages work in all situations except this one, an imageButton inside of a RadGrid used in Safari on an iPad. <br />
<br />
I believe the issue surrounds the use of the RadGrid and the RadAjaxManager.&nbsp; In hopes of improving the problem, I changed the calling function from Response.Redirect to RadAjaxManager1.Redirect().&nbsp; This didn''t have any affect on the behavior.<br />
<br />
Thoughts?', N'Janine Labrune', N'Janine.Labrune@telerik.com', N'PDF opened in Safari iPad via RadGrid can''t get back to RadGrid Page - stuck on PDF', CAST(N'2015-08-31T14:58:00.000' AS DateTime), 1, N'Antonio Morandi', N'Deleted')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (18, N'Hi <span class="messagemaininfo">Jody</span>,<br />
<br />
Thank you for contacting us.<br />
<br />
When the content-disposition for the response is set to attachment there should be a download dialog shown. However, if the browser attempts to open the file this is specific behavior that we cannot control. <br />
<br />
In order to force the file to be opened in external application you should use an approach specific for Safari. Please refer to their support as they could have a way of changing the browser behavior. <br />
<br />
Regards,<br />
Viktor Tachev <br />
Telerik </span></p>
<p><span style="font-size: 9pt; font-family: Arial, sans-serif;">Do you want to have your say when we set our development plans? Do you want to know when a feature you care about is added or when a bug fixed? Explore the <a href="http://www.telerik.com/support/feedback" target="_blank">Telerik Feedback Portal</a> and vote to affect the priority of the items&nbsp;', N'Francisco Chang', N'Francisco.Chang@telerik.com', N'RE: PDF opened in Safari iPad via RadGrid can''t get back to RadGrid Page - stuck on PDF', CAST(N'2015-09-01T07:32:00.000' AS DateTime), 1, N'Antonio Morandi', N'Deleted')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (19, N'Hey there,<br />
<br />
We have a new site that has two blogs:&nbsp;<a href="https://afenafcu.org/about-us/resources/blogs">https://afenafcu.org/about-us/resources/blogs</a>. &nbsp;However, if they use the archive navigation widget it shows the archive listing from all blogs in the sidebar - not just for the particular blog that they''re on. &nbsp;Is there a filter we''re missing in the advanced settings? &nbsp;<br />
<br />
Thanks!<br />
<br />
Landon', N'Ann Devon', N'Ann.Devon@telerik.com', N'Filter Archive Widget by Blog', CAST(N'2015-09-01T07:45:00.000' AS DateTime), 1, N'Antonio Morandi; Viktor Tachev; Ricardo Chirra; Vasil Reav; Mahesh Machineni', N'NativeScript')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (20, N'Hello,<br />
<br />
We have a Sitefinity standard license and I have a question about what we are entitled to in regards to environments/subdomains. I know we are entitled to apprisen.com domain, but wanted to find out the best/supported way to set up development/test environments. Are we entitled to have a dev/test environment for example at an internal dev.apprisen.com or test.apprisen.com? If not, how should we do this? I hope we aren''t required to buy another production license to use for dev/test.&nbsp;<br />
<br />
Thanks,<br />
Justin Rutledge', N'Roland Mendel', N'Roland.Mendel@telerik.com', N'Question about Sitefinity licensing and environments', CAST(N'2015-08-25T09:49:00.000' AS DateTime), 1, N'Antonio Morandi', N'NativeScript')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (21, N'Hello Justin,<br />
<br />
Thank you for using our service. <br />
<br />
I would like to inform you that these questions have been forwarded to our Sitefinity Sales Team. If you have further licenses specific questions/requests you can contact Telerik | Sitefinity Sales &lt;<a href="mailto:sales@sitefinity.com">sales@sitefinity.com</a>&gt; directly.<br />
<br />
Thank you for your patience. One of our colleagues will get back to you shortly.<br />
<br />
Regards,<br />
Stefani Tacheva <br />
Telerik</span></p>
<p><span style="font-size: 9pt; font-family: Arial, sans-serif;">Do you want to have your say in the Sitefinity development roadmap? Do you want to know when a feature you requested is added or when a bug fixed? Explore the <a href="http://feedback.telerik.com/Project/153">Telerik Sitefinity CMS Ideas&amp;Feedback Portal</a> and vote to affect the priority of the items&nbsp;', N'Aria Cruz', N'Aria.Cruz@telerik.com', N'RE: Question about Sitefinity licensing and environments', CAST(N'2015-08-26T05:21:00.000' AS DateTime), 1, N'Antonio Morandi; Vasil Reav', N'NativeScript')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (22, N'Hello Justin,<br />
<br />
With your Sitefinity license you are entitled to unlimited number of development and testing subdomains like the ones you have quoted -&nbsp;dev.apprisen.com and test.apprisen.com. You just need to let us know the exact subdomains you want to use and we will register them for you.<br />
<br />
Looking forward to your reply!<br />
<br />
Regards,<br />
Siyka Kaloferova <br />
Telerik</span></p>
<p><span style="font-size: 9pt; font-family: Arial, sans-serif;">Do you want to have your say in the Sitefinity development roadmap? Do you want to know when a feature you requested is added or when a bug fixed? Explore the <a href="http://feedback.telerik.com/Project/153">Telerik Sitefinity CMS Ideas&amp;Feedback Portal</a> and vote to affect the priority of the items&nbsp;', N'Diego Roel', N'Diego.Roel@telerik.com', N'RE: RE: Question about Sitefinity licensing and environments', CAST(N'2015-08-26T07:47:00.000' AS DateTime), 1, N'Antonio Morandi; Ricardo Chirra', N'NativeScript')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (23, N'Thanks Zach. Can you register dev.apprisen.com and test.apprisen.com?&nbsp;<br />
<br />
Is there a self service option for me to register subdomains for test/dev? I saw a place to register, but it said domains, and not sub. Wanted to clarify.<br />
<br />
Thank you,<br />
Justin', N'Martine Rance', N'Martine.Rancé@telerik.com', N'RE: RE: RE: Question about Sitefinity licensing and environments', CAST(N'2015-08-28T08:55:00.000' AS DateTime), 0, N'Antonio Morandi; Viktor Tachev;', N'KendoUI')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (24, N'Hi,<br />
<br />
Recently we have replaced </span><span style="font-size: 10pt; font-family: Tahoma, sans-serif;">staging database with production database after that we tried sync from staging to&nbsp;production sync is not working .please provide me any suggestions about this one.&nbsp;</span><span style="font-size: 9pt; font-family: Arial, sans-serif;"><br />
</span></p>
<p><span style="font-size: 9pt; font-family: Arial, sans-serif;">Thanks Zach. Can you register dev.apprisen.com and test.apprisen.com?&nbsp;<br />
<br />
Is there a self service option for me to register subdomains for test/dev? I saw a place to register, but it said domains, and not sub. Wanted to clarify.<br />
<br />
Thank you,<br />
Justin', N'Peter Franken', N'Peter.Franken@telerik.com', N'Site sync is not working', CAST(N'2015-08-29T12:15:00.000' AS DateTime), 0, N'Antonio Morandi; Vasil Reav; Ricardo Chirra; Viktor Tachev;', N'KendoUI')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (25, N'Hello,</span><span style="font-size: 9pt; font-family: Arial, sans-serif;"><br />
<br />
<span class="text">Thank you for contacting Sitefinity Support.</span><br />
<br />
<span class="text">We will need some additional time in order to gather information in order to answer your question. We will get back to you as soon as possible with further information. In the meantime, could you send us your Error.log and Synchronization.log files from your project''s App_Data\Sitefinity\Logs\ directory?</span><br />
<br />
<span class="text">Thank you for your patience and understanding.</span><br />
<br />
Regards,<br />
Laurent Poulain <br />
Telerik</span></p>
<p><span style="font-size: 9pt; font-family: Arial, sans-serif;">Do you want to have your say in the Sitefinity development roadmap? Do you want to know when a feature you requested is added or when a bug fixed? Explore the <a href="http://feedback.telerik.com/Project/153">Telerik Sitefinity CMS Ideas&amp;Feedback Portal</a> and vote to affect the priority of the items&nbsp;', N'Carine Schmitt', N'Carine.Schmitt@telerik.com', N'RE: Site sync is not working', CAST(N'2015-08-31T17:22:00.000' AS DateTime), 0, N'Antonio Morandi; Mahesh Machineni; Vasil Reav; Ricardo Chirra; Viktor Tachev;', N'KendoUI')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (26, N'<p>A customer has asked if it would be possible to nest TreeList within a TreeList:</p>
<p><em>nesting a treelist (expand document &lsquo;A&rsquo;- sub tree component apears?)&rdquo;</em></p>
<p>Any insights are greatly appreciated.</p>', N'Lino Rodriguez', N'Lino.Rodriguez@telerik.com', N'Kendo UI TreeList - Nested TreeList?', CAST(N'2014-12-09T00:04:00.000' AS DateTime), 1, N'Antonio Morandi; Mahesh Machineni; Vasil Reav; Ricardo Chirra; Viktor Tachev;', N'KendoUI')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (27, N'No, the TreeList widget doesn''t support any kind of detail template.', N'Eduardo Saavedra', N'Eduardo.Saavedra@telerik.com', N'RE: Kendo UI TreeList - Nested TreeList?', CAST(N'2014-12-09T10:21:00.000' AS DateTime), 0, N'Antonio Morandi; Mahesh Machineni; Vasil Reav; Viktor Tachev;', N'KendoUI')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (28, N'The TreeList indeed doesn&rsquo;t support a detail template. It is used always with a self referencing hierarchy &ndash; all items have the same properties. You can advise the customer to check the grid hierarchy &ndash; it is probably what the customer needs: <a href="http://demos.telerik.com/kendo-ui/grid/hierarchy">http://demos.telerik.com/kendo-ui/grid/hierarchy</a>', N'Howard Snyder', N'Howard.Snyder@telerik.com', N'RE: RE: Kendo UI TreeList - Nested TreeList?', CAST(N'2014-12-09T10:34:00.000' AS DateTime), 0, N'Antonio Morandi; Mahesh Machineni; Vasil Reav; Ricardo Chirra; Viktor Tachev;', N'KendoUI')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (29, N'<p>You have one new comment on your Telerik Kendo UI Q1 2015 Has Landed post</p>
<p>Comment author: Stefan Rahnev</p>
<p>Author e-mail: </p>
<p>Comment text: @Jan, the current service pack release indeed has some issues with VS 2015 RC as it supports MVC6 Beta 3. The good news is we''ll release an updated NuGet package for Telerik UI for ASP.NET MVC this week with support for MVC 6 Beta4 and VS 2015 RC.</p>
<p>Comment date: 5/27/2015 7:13:35 AM</p>
<p>Comment ID: 48d181d2-2b23-6767-8187-ff000040479b</p>', N'Manuel Pereira', N'Manuel.Pereira@telerik.com', N'New Comment: Telerik Kendo UI Q1 2015 Has Landed', CAST(N'2015-05-27T10:14:00.000' AS DateTime), 1, N'Antonio Morandi; Mahesh Machineni; Vasil Reav; Ricardo Chirra; Viktor Tachev;', N'KendoUI')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (30, N'<p>You have one new comment on your Answering Telerik Kendo UI Suite Q3 Webinar Questions post</p>
<p>Comment author: Obat Kaki Gajah</p>
<p>Author e-mail: <a href="mailto:cumaituyangakuberikan@gmail.com">cumaituyangakuberikan@gmail.com</a></p>
<p>Comment text: You selfish you should you want to get their own, it would be difficult and I always give</p>', N'Manuel Pereira', N'Manuel.Pereira@telerik.com', N'New Comment: Answering Telerik Kendo UI Suite Q3 Webinar Questions', CAST(N'2015-05-27T10:51:00.000' AS DateTime), 0, N'Antonio Morandi; Mahesh Machineni; Vasil Reav; Ricardo Chirra;', N'KendoUI')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (31, N'<p>You have one new comment on your Announcing the Telerik Kendo UI Q2 2015 Roadmap post</p>
<p>Comment author: chimon</p>
<p>Author e-mail: <a href="mailto:chimon@outlook.com">iamon@outlook.com</a></p>
<p>Comment text: heyy<br />
Is anyone having a problem with the grid when there is a template editor ng-switch ?</p>
<p>Comment date: 5/27/2015 8:46:25 AM</p>
<p>You have one new comment on your Answering Telerik Kendo UI Suite Q3 Webinar Questions post</p>
<p>Comment author: Obat Kaki Gajah</p>
<p>Author e-mail: <a href="mailto:cumaituyangakuberikan@gmail.com">angakuberikan@gmail.com</a></p>
<p>Comment text: You selfish you should you want to get their own, it would be difficult and I always give</p>', N'Manuel Pereira', N'Manuel.Pereira@telerik.com', N'New Comment: Announcing the Telerik Kendo UI Q2 2015 Roadmap', CAST(N'2015-05-27T11:49:00.000' AS DateTime), 0, N'Antonio Morandi; Mahesh Machineni; Vasil Reav;', N'Sitefinity')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (32, N'<p>You have one new comment on your Announcing the Telerik Kendo UI Q2 2015 Roadmap post</p>
<p>Comment author:iamon</p>
<p>Author e-mail: <a href="mailto:chimon@outlook.com">iamon@outlook.com</a></p>
<p>Comment text: heyy<br />
Is anyone having a problem with the grid when there is a template editor ng-switch ?</p>
<p>You have one new comment on your Announcing the Telerik Kendo UI Q2 2015 Roadmap post</p>
<p>Comment author: iamon</p>
<p>Author e-mail: <a href="mailto:chimon@outlook.com">iamon@outlook.com</a></p>
<p>Comment text: heyy<br />
Is anyone having a problem with the grid when there is a template editor ng-switch ?</p>
<p>Comment date: 5/27/2015 8:46:25 AM</p>
<p>You have one new comment on your Answering Telerik Kendo UI Suite Q3 Webinar Questions post</p>
<p>Comment author: Obat Kaki Gajah</p>
<p>Author e-mail: <a href="mailto:akuberikan@gmail.com">akuberikan@gmail.com</a></p>
<p>Comment text: You selfish you should you want to get their own, it would be difficult and I always give</p>', N'Manuel Pereira', N'Manuel.Pereira@telerik.com', N'New Comment: Announcing the Telerik Kendo UI Q2 2015 Roadmap', CAST(N'2015-05-27T11:55:00.000' AS DateTime), 0, N'Antonio Morandi; Vasil Reav; Ricardo Chirra; Viktor Tachev;', N'Sitefinity')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (33, N'<p>You have one new comment on your The Facts On Using Kendo UI With ASP.NET WebAPI post</p>
<p>Comment author: reza</p>
<p>Author e-mail: <a href="mailto:rezase08@gmail.com">rezase08@gmail.com</a></p>
<p>Comment text: I am using mvc5 and try to load more than 1000 data. But my list view does not show it. Listview remain empty.<br />
<br />
can you give me solution.</p>', N'Manuel Pereira', N'Manuel.Pereira@telerik.com', N'New Comment: The Facts On Using Kendo UI With ASP.NET WebAPI', CAST(N'2015-05-28T23:52:00.000' AS DateTime), 0, N'Antonio Morandi; Mahesh Machineni; Vasil Reav; Ricardo Chirra; Viktor Tachev;', N'Sitefinity')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (34, N'<p>Hi,</p>
<p>All builds now use new shared location.</p>
<p>CI Builds output:</p>
<p><a href="file:///\\telerik.com\distributions\DailyBuilds\NativeScript">\\telerik.com\distributions\DailyBuilds\NativeScript</a></p>
<p>Official releases and release candidates:</p>
<p><a href="file:///\\telerik.com\distributions\OfficialReleases\NativeScript">\\telerik.com\distributions\OfficialReleases\NativeScript</a></p>
<p>Regards,</p>
<p>Dimitar</p>', N'Mario Pontes', N'Mario.Pontes@telerik.com', N'Changes in shared locations', CAST(N'2015-08-13T09:07:00.000' AS DateTime), 1, N'Antonio Morandi; Mahesh Machineni', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (35, N'<p>Hello,</p>
<p>NativeScript&rsquo;s JavaScript security has been discussed internally and asked for by a lot of enterprise prospectors:</p>
<p><a href="https://nativescript.ideas.aha.io/ideas/NS-I-38">https://nativescript.ideas.aha.io/ideas/NS-I-38</a></p>
<p><a href="https://github.com/NativeScript/NativeScript/issues/252">https://github.com/NativeScript/NativeScript/issues/252</a></p>
<p>Although it is not top-voted nobody doubts its priority.</p>
<p>So once we are all aligned internally with the considerations and proposals, we can publish it officially in the GitHub issue and be ready for execution in the upcoming iterations.</p>
<p><strong>Pana</strong></p>
<p>NativeScript for iOS</p>', N'Yoshi Latimer', N'Yoshi.Latimer@telerik.com', N'NativeScript Security', CAST(N'2015-07-06T16:03:00.000' AS DateTime), 1, N'Antonio Morandi; Ricardo Chirra', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (36, N'<p>Hi Lohith,</p>
<p>There is a minor typo in the Kendo UI HTML5/Javascript library named "people.js" located at "examples\content\shared\js\people.js" in line number 5</p>
<p>The titles must spell "Chief Technical Officer", "Chief Executive Officer" instead of "Chief Techical Officer", "Chief Execute Officer"</p>
<p>&lt;original-code&gt;</p>
<p>titles = ["Accountant", "Vice President, Sales", "Sales Representative", "Technical Support", "Sales Manager", "Web Designer",</p>
<p>&nbsp;&nbsp;&nbsp; "Software Developer", "Inside Sales Coordinator", "Chief Techical Officer", "Chief Execute Officer"]</p>
<p>&lt;/original-code&gt;</p>
<p>best,</p>
<p>Shaji</p>
<p>&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;----------&lt;&gt;----------&lt;&gt;</p>
<p>Your talent is God''s gift to you. What you do with it is your gift back to God.</p>
<p>&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;----------&lt;&gt;----------&lt;&gt;</p>', N'Annette Roulet', N'Annette.Roulet@telerik.com', N'Feedback regarding a minor typo in Javascript', CAST(N'2015-08-28T19:13:00.000' AS DateTime), 0, N'Antonio Morandi; Viktor Tachev; Ricardo Chirra', N'Inbox')
GO
INSERT [dbo].[Mails] ([MessageID], [Body], [From], [Email], [Subject], [Received], [IsRead], [To], [Category]) VALUES (37, N'<p>Hi team,</p>
<p>We have a feedback from one of the customer who is evaluating Kendo UI.</p>
<p>They have identified a spelling mistake for the word "Executive". This is&nbsp; in "examples\content\shared\js\people.js". It is currently spelled as "Execute"</p>
<p>Wanted to know if I should raise any issue in GitHub or any pointers on how I can correct the spelling mistake ? May be a pull request.</p>
<p>Thoughts ??</p>
<p>Lohith GN</p>
<p>Developer Evangelist</p>
<p>--------------</p>
<p>Hi Lohith,</p>
<p>There is a minor typo in the Kendo UI HTML5/Javascript library named "people.js" located at "examples\content\shared\js\people.js" in line number 5</p>
<p>The titles must spell "Chief Technical Officer", "Chief Executive Officer" instead of "Chief Techical Officer", "Chief Execute Officer"</p>
<p>&lt;original-code&gt;</p>
<p>titles = ["Accountant", "Vice President, Sales", "Sales Representative", "Technical Support", "Sales Manager", "Web Designer",</p>
<p>&nbsp;&nbsp;&nbsp; "Software Developer", "Inside Sales Coordinator", "Chief Techical Officer", "Chief Execute Officer"]</p>
<p>&lt;/original-code&gt;</p>
<p>best,</p>
<p>Shaji</p>
<p>&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;----------&lt;&gt;----------&lt;&gt;</p>
<p>Your talent is God''s gift to you. What you do with it is your gift back to God.</p>
<p>&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;-----------&lt;&gt;----------&lt;&gt;----------&lt;&gt;</p>', N'Catherine Dewey', N'Catherine.Dewey@telerik.com', N'FW: Feedback regarding a minor typo in Javascript', CAST(N'2015-08-31T08:31:00.000' AS DateTime), 0, N'Antonio Morandi; Vasil Reav', N'Inbox')
GO
SET IDENTITY_INSERT [dbo].[Mails] OFF
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'ALFKI', N'Maria Anders', N'Sales Representative', N'Austria', N'Vienna', N'Alfreds ', N'030-0074321', N'Maria.Anders@telerik.com', N'Favorites')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'ANTON', N'Antonio Moreno', N'Owner', N'Mexico', N'Mexico D.F.', N'Antonio Moreno Taquería', N'(5) 555-3932', N'Antonio.Moreno@telerik.com', N'Favorites')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'BERGS', N'Christina Berglund', N'Order Administrator', N'Sweden', N'Lulea', N'Berglunds snabbköp', N'0921-12 34 65', N'Christina.Berglund@telerik.com', N'Favorites')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'BLAUS', N'Hanna Moos', N'Sales Representative', N'Germany', N'Mannheim', N'Blauer See Delikatessen', N'0621-08460', N'Hanna.Moos@telerik.com', N'Favorites')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'BLONP', N'Frederique Citeaux', N'Marketing Manager', N'France', N'Strasbourg', N'Blondesddsl père et fils', N'88.60.15.31', N'Frédérique.Citeaux@telerik.com', N'Favorites')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'BOLID', N'Martin Sommer', N'Owner', N'Spain', N'Madrid', N'Bólido Comidas preparadas', N'(91) 555 22 82', N'Martín.Sommer@telerik.com', N'Favorites')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'BONAP', N'Laurence Lebihan', N'Owner', N'France', N'Marseille', N'Bon app''', N'91.24.45.40', N'Laurence.Lebihan@telerik.com', N'Favorites')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'BOTTM', N'Elizabeth Lincoln', N'Accounting Manager', N'Canada', N'Tsawassen', N'Bottom-Dollar Markets', N'(604) 555-4729', N'Elizabeth.Lincoln@telerik.com', N'Favorites')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'BSBEV', N'Victoria Ashworth', N'Sales Representative', N'UK', N'London', N'B''s Beverages', N'(171) 555-1212', N'Victoria.Ashworth@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'CACTU', N'Patricio Simpson', N'Sales Agent', N'Argentina', N'Buenos Aires', N'Cactus Comidas para llevar', N'(1) 135-5555', N'Patricio.Simpson@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'CENTC', N'Francisco Chang', N'Marketing Manager', N'Mexico', N'Mexico D.F.', N'Centro comercial Moctezuma', N'(5) 555-3392', N'Francisco.Chang@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'CHOPS', N'Yang Wang', N'Owner', N'Switzerland', N'Bern', N'Chop-suey Chinese', N'0452-076545', N'Yang.Wang@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'COMMI', N'Pedro Afonso', N'Sales Associate', N'Brazil', N'Sao Paulo', N'Comércio Mineiro', N'(11) 555-7647', N'Pedro.Afonso@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'CONSH', N'Elizabeth Brown', N'Sales Representative', N'UK', N'London', N'Consolidated Holdings', N'(171) 555-2282', N'Elizabeth.Brown@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'DRACD', N'Sven Ottlieb', N'Order Administrator', N'Germany', N'Aachen', N'Drachenblut Delikatessen', N'0241-039123', N'Sven.Ottlieb@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'DUMON', N'Janine Labrune', N'Owner', N'France', N'Nantes', N'Du monde entier', N'40.67.88.88', N'Janine.Labrune@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'EASTC', N'Ann Devon', N'Sales Agent', N'UK', N'London', N'Eastern Connection', N'(171) 555-0297', N'Ann.Devon@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'ERNSH', N'Roland Mendel', N'Sales Manager', N'Austria', N'Graz', N'Ernst Handel', N'7675-3425', N'Roland.Mendel@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'FAMIA', N'Aria Cruz', N'Marketing Assistant', N'Brazil', N'Sao Paulo', N'Familia Arquibaldo', N'(11) 555-9857', N'Aria.Cruz@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'FISSA', N'Diego Roel', N'Accounting Manager', N'Spain', N'Madrid', N'FISSA Fabrica Inter. Salchichas S.A.', N'(91) 555 94 44', N'Diego.Roel@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'FOLIG', N'Martine Rance', N'Assistant Sales Agent', N'France', N'Lille', N'Folies gourmandes', N'20.16.10.16', N'Martine.Rancé@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'FOLKO', N'Maria Larsson', N'Owner', N'Sweden', N'Bracke', N'Folk och fä HB', N'0695-34 67 21', N'Maria.Larsson@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'FRANK', N'Peter Franken', N'Marketing Manager', N'Germany', N'München', N'Frankenversand', N'089-0877310', N'Peter.Franken@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'FRANR', N'Carine Schmitt', N'Marketing Manager', N'France', N'Nantes', N'France restauration', N'40.32.21.21', N'Carine.Schmitt@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'FRANS', N'Paolo Accorti', N'Sales Representative', N'Italy', N'Torino', N'Franchi S.p.A.', N'011-4988260', N'Paolo.Accorti@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'FURIB', N'Lino Rodriguez', N'Sales Manager', N'Portugal', N'Lisboa', N'Furia Bacalhau e Frutos do Mar', N'(1) 354-2534', N'Lino.Rodriguez@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'GALED', N'Eduardo Saavedra', N'Marketing Manager', N'Spain', N'Barcelona', N'Galería del gastrónomo', N'(93) 203 4560', N'Eduardo.Saavedra@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'GODOS', N'Jose Pedro Freyre', N'Sales Manager', N'Spain', N'Sevilla', N'Godos Cocina Típica', N'(95) 555 82 82', N'José.Pedro.Freyre@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'GOURL', N'Andre Fonseca', N'Sales Associate', N'Brazil', N'Campinas', N'Gourmet Lanchonetes', N'(11) 555-9482', N'André.Fonseca@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'GREAL', N'Howard Snyder', N'Marketing Manager', N'USA', N'Eugene', N'Great Lakes Food Market', N'(503) 555-7555', N'Howard.Snyder@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'GROSR', N'Manuel Pereira', N'Owner', N'Venezuela', N'Caracas', N'GROSELLA-Restaurante', N'(2) 283-2951', N'Manuel.Pereira@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'HANAR', N'Mario Pontes', N'Accounting Manager', N'Brazil', N'Rio de Janeiro', N'Hanari Carnes', N'(21) 555-0091', N'Mario.Pontes@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'HILAA', N'Carlos Hernandez', N'Sales Representative', N'Venezuela', N'San Cristabal', N'HILARION-Abastos', N'(5) 555-1340', N'Carlos.Hernández@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'HUNGC', N'Yoshi Latimer', N'Sales Representative', N'USA', N'Elgin', N'Hungry Coyote Import Store', N'(503) 555-6874', N'Yoshi.Latimer@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'HUNGO', N'Patricia McKenna', N'Sales Associate', N'Ireland', N'Cork', N'Hungry Owl All-Night Grocers', N'2967 542', N'Patricia.McKenna@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'ISLAT', N'Helen Bennett', N'Marketing Manager', N'UK', N'Cowes', N'Island Trading', N'(198) 555-8888', N'Helen.Bennett@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'KOENE', N'Philip Cramer', N'Sales Associate', N'Germany', N'Brandenburg', N'Königlich Essen', N'0555-09876', N'Philip.Cramer@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'LACOR', N'Daniel Tonini', N'Sales Representative', N'France', N'Versailles', N'La corne d''abondance', N'30.59.84.10', N'Daniel.Tonini@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'LAMAI', N'Annette Roulet', N'Sales Manager', N'France', N'Toulouse', N'La maison d''Asie', N'61.77.61.10', N'Annette.Roulet@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'LAUGB', N'Yoshi Tannamuri', N'Marketing Assistant', N'Canada', N'Vancouver', N'Laughing Bacchus Wine Cellars', N'(604) 555-3392', N'Yoshi.Tannamuri@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'LAZYK', N'John Steel', N'Marketing Manager', N'USA', N'Walla Walla', N'Lazy K Kountry Store', N'(509) 555-7969', N'John.Steel@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'LEHMS', N'Renate Messner', N'Sales Representative', N'Germany', N'Frankfurt a.M.', N'Lehmanns Marktstand', N'069-0245984', N'Renate.Messner@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'LETSS', N'Jaime Yorres', N'Owner', N'USA', N'San Francisco', N'Let''s Stop N Shop', N'(415) 555-5938', N'Jaime.Yorres@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'LILAS', N'Carlos Gonzalez', N'Accounting Manager', N'Venezuela', N'Barquisimeto', N'LILA-Supermercado', N'(9) 331-6954', N'Carlos.González@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'LINOD', N'Felipe Izquierdo', N'Owner', N'Venezuela', N'I. de Margarita', N'LINO-Delicateses', N'(8) 34-56-12', N'Felipe.Izquierdo@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'LONEP', N'Fran Wilson', N'Sales Manager', N'USA', N'Portland', N'Lonesome Pine Restaurant', N'(503) 555-9573', N'Fran.Wilson@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'MAGAA', N'Giovanni Rovelli', N'Marketing Manager', N'Italy', N'Bergamo', N'Magazzini Alimentari Riuniti', N'035-640230', N'Giovanni.Rovelli@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'MAISD', N'Catherine Dewey', N'Sales Agent', N'Belgium', N'Bruxelles', N'Maison Dewey', N'(02) 201 24 67', N'Catherine.Dewey@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'MEREP', N'Jean Fresniere', N'Marketing Assistant', N'Canada', N'Montreal', N'Mère Paillarde', N'(514) 555-8054', N'Jean.Fresnière@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'MORGK', N'Alexander Feuer', N'Marketing Assistant', N'Germany', N'Leipzig', N'Morgenstern Gesundkost', N'0342-023176', N'Alexander.Feuer@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'NORTS', N'Simon Crowther', N'Sales Associate', N'UK', N'London', N'North/South', N'(171) 555-7733', N'Simon.Crowther@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'OCEAN', N'Yvonne Moncada', N'Sales Agent', N'Argentina', N'Buenos Aires', N'Ocaano Atlantico Ltda.', N'(1) 135-5333', N'Yvonne.Moncada@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'OLDWO', N'Rene Phillips', N'Sales Representative', N'USA', N'Anchorage', N'Old World Delicatessen', N'(907) 555-7584', N'Rene.Phillips@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'OTTIK', N'Henriette Pfalzheim', N'Owner', N'Germany', N'Köln', N'Ottilies Kaseladen', N'0221-0644327', N'Henriette.Pfalzheim@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'PARIS', N'Marie Bertrand', N'Owner', N'France', N'Paris', N'Paris spacialites', N'(1) 42.34.22.66', N'Marie.Bertrand@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'PERIC', N'Guillermo Fernandez', N'Sales Representative', N'Mexico', N'Mexico D.F.', N'Pericles Comidas clásicas', N'(5) 552-3745', N'Guillermo.Fernández@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'PICCO', N'Georg Pipps', N'Sales Manager', N'Austria', N'Salzburg', N'Piccolo und mehr', N'6562-9722', N'Georg.Pipps@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'PRINI', N'Isabel de Castro', N'Sales Representative', N'Portugal', N'Lisboa', N'Princesa Isabel Vinhos', N'(1) 356-5634', N'Isabel.de.Castro@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'QUEDE', N'Bernardo Batista', N'Accounting Manager', N'Brazil', N'Rio de Janeiro', N'Que Delícia', N'(21) 555-4252', N'Bernardo.Batista@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'QUEEN', N'Lucia Carvalho', N'Marketing Assistant', N'Brazil', N'Sao Paulo', N'Queen Cozinha', N'(11) 555-1189', N'Lúcia.Carvalho@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'QUICK', N'Horst Kloss', N'Accounting Manager', N'Germany', N'Cunewalde', N'QUICK-Stop', N'0372-035188', N'Horst.Kloss@telerik.com', N'Friends')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'RANCH', N'Sergio Gutiérrez', N'Sales Representative', N'Argentina', N'Buenos Aires', N'Rancho grande', N'(1) 123-5555', N'Sergio.Gutiérrez@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'RATTC', N'Paula Wilson', N'Assistant Sales Representative', N'USA', N'Albuquerque', N'Rattlesnake Canyon Grocery', N'(505) 555-5939', N'Paula.Wilson@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'REGGC', N'Maurizio Moroni', N'Sales Associate', N'Italy', N'Reggio Emilia', N'Reggiani Caseifici', N'0522-556721', N'Maurizio.Moroni@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'RICAR', N'Janete Limeira', N'Assistant Sales Agent', N'Brazil', N'Rio de Janeiro', N'Ricardo Adocicados', N'(21) 555-3412', N'Janete.Limeira@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'RICSU', N'Michael Holz', N'Sales Manager', N'Switzerland', N'Genève', N'Richter Supermarkt', N'0897-034214', N'Michael.Holz@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'ROMEY', N'Alejandra Camino', N'Accounting Manager', N'Spain', N'Madrid', N'Romero y tomillo', N'(91) 745 6200', N'Alejandra.Camino@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'SANTG', N'Jonas Bergulfsen', N'Owner', N'Norway', N'Stavern', N'Sante Gourmet', N'07-98 92 35', N'Jonas.Bergulfsen@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'SAVEA', N'Jose Pavarotti', N'Sales Representative', N'USA', N'Boise', N'Save-a-lot Markets', N'(208) 555-8097', N'Jose.Pavarotti@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'SEVES', N'Hari Kumar', N'Sales Manager', N'UK', N'London', N'Seven Seas Imports', N'(171) 555-1717', N'Hari.Kumar@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'SIMOB', N'Jytte Petersen', N'Owner', N'Denmark', N'Kobenhavn', N'Simons bistro', N'31 12 34 56', N'Jytte.Petersen@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'SPECD', N'Dominique Perrier', N'Marketing Manager', N'France', N'Paris', N'Specialites du monde', N'(1) 47.55.60.10', N'Dominique.Perrier@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'SPLIR', N'Art Braunschweiger', N'Sales Manager', N'USA', N'Lander', N'Split Rail Beer & Ale', N'(307) 555-4680', N'Art.Braunschweiger@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'SUPRD', N'Pascale Cartrain', N'Accounting Manager', N'Belgium', N'Charleroi', N'Supremes delices', N'(071) 23 67 22 20', N'Pascale.Cartrain@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'THEBI', N'Liz Nixon', N'Marketing Manager', N'USA', N'Portland', N'The Big Cheese', N'(503) 555-3612', N'Liz.Nixon@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'THECR', N'Liu Wong', N'Marketing Assistant', N'USA', N'Butte', N'The Cracker Box', N'(406) 555-5834', N'Liu.Wong@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'TOMSP', N'Karin Josephs', N'Marketing Manager', N'Germany', N'Münster', N'Toms Spezialitaten', N'0251-031259', N'Karin.Josephs@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'TORTU', N'Miguel Angel Paolino', N'Owner', N'Mexico', N'Mexico D.F.', N'Tortuga Restaurante', N'(5) 555-2933', N'Miguel.Angel.Paolino@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'TRADH', N'Anabela Domingues', N'Sales Representative', N'Brazil', N'Sao Paulo', N'Tradição Hipermercados', N'(11) 555-2167', N'Anabela.Domingues@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'TRAIH', N'Helvetius Nagy', N'Sales Associate', N'USA', N'Kirkland', N'Trail''s Head Gourmet Provisioners', N'(206) 555-8257', N'Helvetius.Nagy@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'VAFFE', N'Palle Ibsen', N'Sales Manager', N'Denmark', N'Arhus', N'Vaffeljernet', N'86 21 32 43', N'Palle.Ibsen@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'VICTE', N'Mary Saveley', N'Sales Agent', N'France', N'Lyon', N'Victuailles en stock', N'78.32.54.86', N'Mary.Saveley@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'VINET', N'Paul Henriot', N'Accounting Manager', N'France', N'Reims', N'Vins et alcools Chevalier', N'26.47.15.10', N'Paul.Henriot@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'WANDK', N'Rita Müller', N'Sales Representative', N'Germany', N'Stuttgart', N'Die Wandernde Kuh', N'0711-020361', N'Rita.Müller@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'WARTH', N'Pirkko Koskitalo', N'Accounting Manager', N'Finland', N'Oulu', N'Wartian Herkku', N'981-443655', N'Pirkko.Koskitalo@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'WELLI', N'Paula Parente', N'Sales Manager', N'Brazil', N'Resende', N'Wellington Importadora', N'(14) 555-8122', N'Paula.Parente@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'WHITC', N'Karl Jablonski', N'Owner', N'USA', N'Seattle', N'White Clover Markets', N'(206) 555-4112', N'Karl.Jablonski@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'WILMK', N'Matti Karttunen', N'Owner/Marketing Assistant', N'Finland', N'Helsinki', N'Wilman Kala', N'90-224 8858', N'Matti.Karttunen@telerik.com', N'Work')
GO
INSERT [dbo].[People] ([Id], [Name], [Title], [Country], [City], [Company], [Phone], [Email], [Category]) VALUES (N'WOLZA', N'Zbyszek Piestrzeniewicz', N'Owner', N'Poland', N'Warszawa', N'Wolski  Zajazd', N'(26) 642-7012', N'Zbyszek.Piestrzeniewicz@telerik.com', N'Work')
GO
SET IDENTITY_INSERT [dbo].[Tasks] ON 

GO
INSERT [dbo].[Tasks] ([Id], [Subject], [DateCreated], [NoteContent], [Category]) VALUES (2, N'Submit whitepaper proposal', CAST(N'2015-08-31T18:36:00.000' AS DateTime), N'Submit whitepaper proposal', N'All')
GO
INSERT [dbo].[Tasks] ([Id], [Subject], [DateCreated], [NoteContent], [Category]) VALUES (3, N'Buy birthday present for Jill', CAST(N'2015-08-31T18:36:00.000' AS DateTime), N'Buy birthday present for Jill', N'All')
GO
INSERT [dbo].[Tasks] ([Id], [Subject], [DateCreated], [NoteContent], [Category]) VALUES (4, N'Weekly data backup', CAST(N'2015-08-31T18:36:00.000' AS DateTime), N'Weekly data backup', N'Personal')
GO
INSERT [dbo].[Tasks] ([Id], [Subject], [DateCreated], [NoteContent], [Category]) VALUES (6, N'Reserve table at ANW Vegetarian Restaurant', CAST(N'2015-08-31T18:36:00.000' AS DateTime), N'Reserve table at ANW Vegetarian Restaurant', N'Personal')
GO
INSERT [dbo].[Tasks] ([Id], [Subject], [DateCreated], [NoteContent], [Category]) VALUES (7, N'Pick up exam results from the clinic', CAST(N'2015-08-31T18:36:00.000' AS DateTime), N'Pick up exam results from the clinic', N'Personal')
GO
INSERT [dbo].[Tasks] ([Id], [Subject], [DateCreated], [NoteContent], [Category]) VALUES (8, N'Order the new JavaScript book on Amazon', CAST(N'2015-08-31T18:36:00.000' AS DateTime), N'Order the new JavaScript book on Amazon', N'Personal')
GO
INSERT [dbo].[Tasks] ([Id], [Subject], [DateCreated], [NoteContent], [Category]) VALUES (10, N'Send a cancellation for the planning meeting', CAST(N'2015-08-31T18:36:00.000' AS DateTime), N'Send a cancellation for the planning meeting', N'Personal')
GO
INSERT [dbo].[Tasks] ([Id], [Subject], [DateCreated], [NoteContent], [Category]) VALUES (11, N'Buy tickets for Madonna''s concert', CAST(N'2015-08-31T18:36:00.000' AS DateTime), N'Buy tickets for Madonna''s concert', N'Work')
GO
INSERT [dbo].[Tasks] ([Id], [Subject], [DateCreated], [NoteContent], [Category]) VALUES (13, N'Take Samantha to the annual art exhibition', CAST(N'2015-08-31T18:36:00.000' AS DateTime), N'Take Samantha to the annual art exhibition', N'Work')
GO
INSERT [dbo].[Tasks] ([Id], [Subject], [DateCreated], [NoteContent], [Category]) VALUES (14, N'Check account balance', CAST(N'2015-08-31T18:36:00.000' AS DateTime), N'Check account balance', N'Work')
GO
SET IDENTITY_INSERT [dbo].[Tasks] OFF
GO
/****** Object:  Index [IX_FK_Tasks_Tasks]    Script Date: 5/8/2017 8:58:23 AM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Events]') AND name = N'IX_FK_Tasks_Tasks')
CREATE NONCLUSTERED INDEX [IX_FK_Tasks_Tasks] ON [dbo].[Events]
(
	[RecurrenceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tasks_Tasks]') AND parent_object_id = OBJECT_ID(N'[dbo].[Events]'))
ALTER TABLE [dbo].[Events]  WITH CHECK ADD  CONSTRAINT [FK_Tasks_Tasks] FOREIGN KEY([RecurrenceID])
REFERENCES [dbo].[Events] ([EventID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tasks_Tasks]') AND parent_object_id = OBJECT_ID(N'[dbo].[Events]'))
ALTER TABLE [dbo].[Events] CHECK CONSTRAINT [FK_Tasks_Tasks]
GO
USE [master]
GO
ALTER DATABASE [OAGNCircular] SET  READ_WRITE 
GO
