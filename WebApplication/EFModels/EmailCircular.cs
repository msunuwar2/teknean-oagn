﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelerikMvcWebMail.EFModels
{
    public class EmailCircular
    {
        public string Source { get; set; }
        public string Destination { get; set; }

        public string RefNo { get; set; }
        public string DispatchNo { get; set; }
        public string Subject { get; set; }
        public DateTime Date { get; set; }

        public string Content { get; set; }
        public string Attachment { get; set; }
        [ScaffoldColumn(false)]
        public DateTime CreatedDate { get; set; }

        [MaxLength(256)]
        [ScaffoldColumn(false)]
        public string CreatedBy { get; set; }

        [ScaffoldColumn(false)]
        public DateTime UpdatedDate { get; set; }

        [MaxLength(256)]
        [ScaffoldColumn(false)]
        public string UpdatedBy { get; set; }
        public virtual int Id { get; set; }
    }
}
