﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelerikMvcWebMail.EFModels;
using WebApplication.Models;

namespace TelerikMvcWebMail.Models
{
    public class CircularService
    {
        private ApplicationDbContext _context;

        public CircularService(ApplicationDbContext applicationDbContext)
        {
            this._context = applicationDbContext;
        }

        public IQueryable<EmailCircular> GetAll()
        {
            IQueryable<EmailCircular> result = _context.EmailCircular;

            result = _context.EmailCircular.Select(
                e => new EmailCircular
                {
                    Id = e.Id,
                    Content = e.Content,
                    Attachment = e.Attachment,
                    CreatedBy = e.CreatedBy,
                    Date = e.Date,
                    Destination = e.Destination,
                    Subject = e.Subject,
                    Source = e.Source,
                    RefNo = e.RefNo,
                    DispatchNo = e.DispatchNo

                }
                );
            return result;
        }


        public IEnumerable<EmailCircular> Read()
        {
            return GetAll();
        }

     
        public void Insert(EmailCircular contact)
        {
                _context.EmailCircular.Add(contact);
                _context.SaveChanges();

                   
        }

     
        public void Delete(EmailCircular contact)
        {
       
                  _context.EmailCircular.Remove(contact);
                   _context.SaveChanges();
              
        }

  

    }
}

