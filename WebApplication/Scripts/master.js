﻿$('#tn-toggle-sidebar').on("click", function () {

    $(".tn-sidebar").toggleClass("display-none");
    $(".tn-body-content").toggleClass("tn-body-content-toggle");

});

$(window).on("resize", function () {
    if ($(window).width() > 751) {
        $(".tn-sidebar").removeClass("hidden");
    }
    else {
        $(".tn-sidebar").addClass("hidden");
    }
})